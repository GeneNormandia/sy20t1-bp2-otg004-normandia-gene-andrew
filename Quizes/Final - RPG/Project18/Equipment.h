#pragma once
#include <string>
#include <iostream>

using namespace std;

class Equipment
{
public:
	Equipment(int power, string name);
	string getName();
	int getItemPower();

private:
	int mItemPower;
	string mName;
};

