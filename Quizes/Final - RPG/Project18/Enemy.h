#pragma once
#include <string>
#include "Unit.h"

class Player;

class Enemy :
    public Unit
{
public:
    Enemy(int classNumber);
    void printStats() override;
    void attack(Unit* target) override;
    int getVit();
};