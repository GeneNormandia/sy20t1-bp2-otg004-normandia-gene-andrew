#include <iostream>
#include <string>
#include "Player.h"
#include "Equipment.h"
#include "Enemy.h"

Player::Player(string name, int classNumber)
	: Unit()
{
	mName = name;

	if (classNumber == 1) // warrior has high POW & better starting weapon
	{
		mClass = "Warrior";

		mMaxHp = 120;
		mHp = 120;
		mPow = 20;
		mVit = 3;
		mAgi = 5;
		mDex = 2;
		mWeapon = new Equipment(5, "Knight Sword");
		mArmor = new Equipment(1, "Novice Armor");
	}

	else if (classNumber == 2) // thief has high DEX & AGI
	{
		mClass = "Thief";

		mMaxHp = 120;
		mHp = 120;
		mPow = 13;
		mVit = 3;
		mAgi = 7;
		mDex = 5;
		mWeapon = new Equipment(3, "Novice Sword");
		mArmor = new Equipment(1, "Novice Armor");
	}

	else if (classNumber == 3) // crusader has high HP & VIT
	{
		mClass = "Crusader";

		mMaxHp = 220;
		mHp = 220;
		mPow = 13;
		mVit = 13;
		mAgi = 5;
		mDex = 2;
		mWeapon = new Equipment(3, "Novice Sword");
		mArmor = new Equipment(1, "Novice Armor");
	}
}

Player::~Player()
{
	delete mArmor;
	delete mWeapon;
}

string Player::getName()
{
	return mName;
}

int Player::getVitArmor()
{
	return mVit + mArmor->getItemPower();
}

int Player::getLevel()
{
	return mLevel;
}

int Player::getExp()
{
	return mExp;
}

int Player::getX()
{
	return x;
}

int Player::getY()
{
	return y;
}

void Player::move()
{
	int moveTo;
	bool isDirectionValid = false;
	isDirectionValid = false;

	while (isDirectionValid == false)
	{
		cout << "Current location: (" << x << ", " << y << ")" << endl;
		cout << endl;
		cout << "Where do you want to go?" << endl;
		cout << "1) North, 2) South, 3) West, 4) East" << endl;
		cin >> moveTo;

		if (moveTo == 1) { y += 1; isDirectionValid = true; }
		if (moveTo == 2) { y -= 1; isDirectionValid = true; }
		if (moveTo == 3) { x -= 1; isDirectionValid = true; }
		if (moveTo == 4) { x += 1; isDirectionValid = true; }
	}
	cout << "Moved to (" << x << ", " << y << ")" << endl;
	cout << endl;
	system("pause");
	system("cls");
}

void Player::rest()
{
	cout << "You recovered your full HP." << endl;
	mHp = mMaxHp; // restore hp to maxHp
	cout << endl;
	system("pause");
	system("cls");
}

void Player::printStats()
{
	cout << "PLAYER STATS:" << endl;
	cout << endl;
	cout << "Name: " << mName << endl;
	cout << "Class: " << mClass << endl;
	cout << "Level: " << mLevel << endl;
	cout << "Current Exp: " << mExp << endl;
	cout << "Gold: " << mGold << endl;
	cout << "Armor: " << mArmor->getName() << " (+" << mArmor->getItemPower() << " Armor)" << endl;
	cout << "Weapon: " << mWeapon->getName() << " (+" << mWeapon->getItemPower() << " Damage)" << endl;
	cout << endl;
	cout << "HP: " << mHp << endl;
	cout << "Power: " << mPow << endl;
	cout << "Vitality: " << mVit << endl;
	cout << "Agility: " << mAgi << endl;
	cout << "Dexterity: " << mDex << endl;
	cout << "--------------------------" << endl;
}

void Player::enterCombat(Enemy* enemy)
{
	int attackChoice;
	int probability = rand() % 100 + 1;

	cout << "What will you do?" << endl;
	cout << "1) Attack" << endl;
	cout << "2) Run Away" << endl;
	cin >> attackChoice;
	system("cls");

	// if player chose to attack
	if (attackChoice == 1)
	{
		while (mHp > 0 && enemy->getHp() > 0)
		{
			cout << "COMBAT:" << endl;
			cout << endl;
			cout << "Player HP: " << mHp << endl;
			cout << enemy->getClass() << " HP: " << enemy->getHp() << endl;
			cout << endl;
			this->attack(enemy);
			system("pause");

			if (enemy->getHp() > 0) // prevents enemy from attacking soon as it dies
			{
				cout << endl;
				enemy->attack(this);
				system("pause");
			}

			system("cls");
		}

		if (this->mHp > 0) // call rewards if player still alive
		{
			this->getExpGold(enemy);
			system("pause");
			system("cls");
		}
	}

	// if player chose to run
	if (attackChoice == 2)
	{
		if (probability >= 1 && probability <= 25)
		{
			cout << "You have escaped from the enemy." << endl;
			system("pause");
			system("cls");
		}

		else
		{
			cout << "You failed to run away and the enemy attacked you." << endl;
			enemy->attack(this); // enemy will attack & player will not attack this turn
			system("pause");
			system("cls");

			while (mHp > 0 && enemy->getHp() > 0)
			{
				cout << "COMBAT:" << endl;
				cout << endl;
				cout << "Player HP: " << mHp << endl;
				cout << enemy->getClass() << " HP: " << enemy->getHp() << endl;
				cout << endl;
				this->attack(enemy);
				system("pause");

				if (enemy->getHp() > 0) // prevents enemy from attacking soon as it dies
				{
					cout << endl;
					enemy->attack(this);
					system("pause");
				}

				system("cls");
			}

			if (this->mHp > 0) // call rewards if player still alive
			{
				this->getExpGold(enemy);
				system("pause");
				system("cls");
			}
		}
	}
}

void Player::attack(Unit* target)
{
	Enemy* enemy = (Enemy*)target;

	int hitRate = ((float)mDex / (float)enemy->getAgi()) * 100;
	int probability = rand() % 100 + 1;
	if (hitRate < 20) hitRate = 20; // hit rate shouldn't be lower than 20%
	else if (hitRate > 80) hitRate = 80; // and musn't exceed 80%

	if (probability >= 1 && probability <= hitRate) // compute dmg if hit
	{
		int damage = ((mPow + mWeapon->getItemPower()) - enemy->getVit());
		if (damage <= 0) damage = 1;

		enemy->setHp(damage);
		cout << mName << " has attacked the enemy " << enemy->getClass() << " for " << damage << "!" << endl;
	}

	else if (probability > hitRate)
	{
		cout << mName << " missed!" << endl;
	}
}

void Player::getExpGold(Enemy* target)
{
	int requiredExp = mLevel * 1000;

	if (target->getClass() == "Goblin") // rewards if you defeated a goblin
	{
		cout << "You defeated the goblin!" << endl;
		cout << endl;
		cout << "Rewards: Exp +100, Gold +10" << endl;
		mExp += 100;
		mGold += 10;
	}

	if (target->getClass() == "Ogre") // rewards if you defeated a ogre
	{
		cout << "You defeated the ogre!" << endl;
		cout << endl;
		cout << "Rewards: Exp +250, Gold +50" << endl;
		mExp += 250;
		mGold += 50;
	}

	if (target->getClass() == "Orc") // rewards if you defeated a orc
	{
		cout << "You defeated the orc!" << endl;
		cout << endl;
		cout << "Rewards: Exp +500, Gold +100" << endl;
		mExp += 500;
		mGold += 100;
	}

	if (target->getClass() == "Orc Lord") // rewards if you defeated a orc lord
	{
		cout << "You defeated the orc lord!" << endl;
		cout << endl;
		cout << "Rewards: Exp +1000, Gold +1000" << endl;
		mExp += 1000;
		mGold += 1000;
	}

	// check if current exp reached the requiredExp
	if (mExp >= requiredExp)
	{
		cout << "LEVEL UP!" << endl;
		cout << endl;

		if (this->mClass == "Warrior") // warrior gets higher pow rewards
		{
			mMaxHp += 10;
			int addPow = rand() % (4 + 1) + 2;
			mPow += addPow;
			int addVit = rand() % 3;
			mVit += addVit;
			int addAgi = rand() % 1;
			mAgi += addAgi;
			int addDex = rand() % 1;
			mDex += addDex;

			cout << "Your stats have increased: " << endl;
			cout << "Max HP +" << 10 << endl;
			cout << "Power +" << addPow << endl;
			cout << "Vitality +" << addVit << endl;
			cout << "Agility +" << addAgi << endl;
			cout << "Dexterity +" << addDex << endl;
		}

		if (this->mClass == "Thief") // thief gets higher dex & agi rewards
		{
			mMaxHp += 10;
			int addPow = rand() % 3 + 1;
			mPow += addPow;
			int addVit = rand() % 3;
			mVit += addVit;
			int addAgi = rand() % 2 + 1;
			mAgi += addAgi;
			int addDex = rand() % 2 + 1;
			mDex += addDex;

			cout << "Your stats have increased: " << endl;
			cout << "Max HP +" << 10 << endl;
			cout << "Power +" << addPow << endl;
			cout << "Vitality +" << addVit << endl;
			cout << "Agility +" << addAgi << endl;
			cout << "Dexterity +" << addDex << endl;
		}

		if (this->mClass == "Crusader") // crusader gets higher HP & vit rewards
		{
			mMaxHp += 20;
			int addPow = rand() % 3 + 1;
			mPow += addPow;
			int addVit = rand() % 7 + 1;
			mVit += addVit;
			int addAgi = rand() % 1;
			mAgi += addAgi;
			int addDex = rand() % 1;
			mDex += addDex;

			cout << "Your stats have increased: " << endl;
			cout << "Max HP +" << 20 << endl;
			cout << "Power +" << addPow << endl;
			cout << "Vitality +" << addVit << endl;
			cout << "Agility +" << addAgi << endl;
			cout << "Dexterity +" << addDex << endl;
		}

		mExp -= requiredExp; // carry over excess exp to next lvl
		mLevel++; // increase lvl
	}
}

void Player::enterShop()
{
	int shopMenu;
	int itemToBuy;
	bool isStoreActive = true;

	while (isStoreActive == true)
	{
		cout << "Welcome to the Shop!" << endl;
		cout << endl;
		cout << "Current Gold: " << mGold << endl;
		cout << "What would you like to do?" << endl;
		cout << "1) Buy Weapons" << endl;
		cout << "2) Buy Armors" << endl;
		cout << "3) Leave Shop" << endl;
		cin >> shopMenu;

		// buy weapon
		if (shopMenu == 1)
		{
			cout << "Weapons:" << endl;
			cout << "1) Short Sword: 5 Damage (10 Gold)" << endl;
			cout << "2) Long Sword: 10 Damage (50 Gold)" << endl;
			cout << "3) Broad Sword: 20 Damage (200 Gold)" << endl;
			cout << "4) Excalibur: 999 Damage (9999 Gold)" << endl;
			cin >> itemToBuy;

			if (itemToBuy == 1 && mGold >= 10)
			{
				mGold -= 10;
				cout << "You bought a Short Sword" << endl;
				changeWeapon("Short Sword");
			}

			else if (itemToBuy == 2 && mGold >= 50)
			{
				mGold -= 50;
				cout << "You bought a Long Sword" << endl;
				changeWeapon("Long Sword");
			}

			else if (itemToBuy == 3 && mGold >= 200)
			{
				mGold -= 200;
				cout << "You bought a Broad Sword" << endl;
				changeWeapon("Broad Sword");
			}

			else if (itemToBuy == 4 && mGold >= 9999)
			{
				mGold -= 9999;
				cout << "You bought Excalibur" << endl;
				changeWeapon("Excalibur");
			}

			else { cout << "You don't have enough gold to buy that!" << endl; }

			system("pause");
			system("cls");
		}

		// buy armor
		else if (shopMenu == 2)
		{
			cout << "Armor:" << endl;
			cout << "1) Leather Mail: 2 Armor (50 Gold)" << endl;
			cout << "2) Chain Mail: 4 Armor (100 Gold)" << endl;
			cout << "3) Plate Armor: 8 Armor (300 Gold)" << endl;
			cin >> itemToBuy;

			if (itemToBuy == 1 && mGold >= 50)
			{
				mGold -= 50;
				cout << "You bought a Leather Mail" << endl;
				changeArmor("Leather Mail");
			}

			else if (itemToBuy == 2 && mGold >= 100)
			{
				mGold -= 100;
				cout << "You bought a Chain Mail" << endl;
				changeArmor("Chain Mail");
			}

			else if (itemToBuy == 3 && mGold >= 300)
			{
				mGold -= 300;
				cout << "You bought a Plate Armor" << endl;
				changeArmor("Plate Armor");
			}

			else { cout << "You don't have enough gold to buy that!" << endl; }

			system("pause");
			system("cls");
		}

		// leave shop
		else if (shopMenu == 3)
		{
			cout << "You left the shop." << endl;
			isStoreActive = false;
			system("pause");
			system("cls");
		}
	}
}

void Player::changeArmor(string name)
{
	// delete the previous armor
	if (mArmor != nullptr) delete mArmor;

	// equip the new armor
	if (name == "Leather Mail")
	{
		mArmor = new Equipment(2, "Leather Mail");
	}

	if (name == "Chain Mail")
	{
		mArmor = new Equipment(4, "Chain Mail");
	}

	if (name == "Plate Armor")
	{
		mArmor = new Equipment(8, "Plate Armor");
	}

	cout << mArmor->getName() << " is equipped." << endl;
}

void Player::changeWeapon(string name)
{
	// delete the previous weapon
	if (mWeapon != nullptr) delete mWeapon;

	// equip the new weapon
	if (name == "Short Sword")
	{
		mWeapon = new Equipment(5, "Short Sword");
	}

	if (name == "Long Sword")
	{
		mWeapon = new Equipment(10, "Long Sword");
	}

	if (name == "Broad Sword")
	{
		mWeapon = new Equipment(20, "Broad Sword");
	}

	if (name == "Excalibur")
	{
		mWeapon = new Equipment(999, "Excalibur");
	}

	cout << mWeapon->getName() << " is equipped." << endl;
}
