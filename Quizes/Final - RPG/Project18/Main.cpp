#include <iostream>
#include <string>
#include <time.h>
#include "Unit.h"
#include "Player.h"
#include "Enemy.h"

using namespace std;

void checkEncounter(Player* player);

int main()
{
	srand(time(NULL));

	int mainMenu;
	bool isQuitted = false;

	// create player
	string name;
	int classNumber;

	cout << "Please enter your name:" << endl;
	cin >> name;
	cout << "Please choose your class: " << endl;
	cout << "1) Warrior, 2) Thief, 3) Crusader" << endl;
	cin >> classNumber;
	Player* player = new Player(name, classNumber);
	system("pause");
	system("cls");

	// MAIN MENU
	while (player->getHp() > 0 && isQuitted == false)
	{
		cout << "================ MAIN MENU ================" << endl;
		cout << "Name: " << player->getName() << endl;
		cout << "Current location: (" << player->getX() << ", " << player->getY() << ")" << endl;
		cout << "HP: " << player->getHp() << endl;
		cout << endl;
		cout << "1) Move" << endl;
		cout << "2) Rest" << endl;
		cout << "3) View Stats" << endl;
		cout << "4) Quit Game" << endl;
		cin >> mainMenu;
		system("cls");

		// Move
		if (mainMenu == 1)
		{
			player->move();

			// check if player is at store
			if (player->getX() == 1 && player->getY() == 1)
			{
				player->enterShop();
			}

			// spawn an enemy otherwise
			else
			{
				checkEncounter(player);
			}
		}

		// Rest
		if (mainMenu == 2)
		{
			player->rest();
		}

		// View Stats
		if (mainMenu == 3)
		{
			player->printStats();
			system("pause");
			system("cls");
		}

		// Quit
		if (mainMenu == 4)
		{
			cout << "You quit the game." << endl;
			isQuitted = true;
		}
	}

	if (player->getHp() <= 0) // defeat condition
	{
		cout << "You have died!" << endl;
	}

	delete player;

	system("pause");
	return 0;
}

void checkEncounter(Player* player)
{
	// check enemy spawner
	int probability = rand() % 100 + 1;

	// 20% no monster spawning
	if (probability >= 1 && probability <= 20)
	{
		cout << "No monsters encountered." << endl;
		cout << endl;
		system("pause");
		system("cls");
	}

	// 25% goblin
	else if (probability >= 21 && probability <= 45)
	{
		Enemy* enemy = new Enemy(1);
		enemy->printStats();
		cout << "You've encountered a wild Goblin!" << endl;
		player->enterCombat(enemy);

		delete enemy;
	}

	// 25% ogre
	else if (probability >= 46 && probability <= 70)
	{
		Enemy* enemy = new Enemy(2);
		enemy->printStats();
		cout << "You've encountered a wild Ogre!" << endl;
		player->enterCombat(enemy);

		delete enemy;
	}

	// 25% orc
	else if (probability >= 71 && probability <= 95)
	{
		Enemy* enemy = new Enemy(3);
		enemy->printStats();
		cout << "You've encountered a wild Orc!" << endl;
		player->enterCombat(enemy);

		delete enemy;
	}

	// 5% orc lord
	else if (probability >= 96 && probability <= 100)
	{
		Enemy* enemy = new Enemy(4);
		enemy->printStats();
		cout << "You've encountered a wild Orc Lord!" << endl;
		player->enterCombat(enemy);

		delete enemy;
	}

}