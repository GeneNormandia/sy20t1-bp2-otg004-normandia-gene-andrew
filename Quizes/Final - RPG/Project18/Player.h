#pragma once
#include <string>
#include "Unit.h"

using namespace std;

class Equipment;
class Enemy;

class Player :
    public Unit
{
public:
    // constructor & destructor
    Player(string name, int classNumber);
    ~Player();

    // accessors
    string getName();
    int getVitArmor();
    int getLevel();
    int getExp();
    int getX();
    int getY();

    // behaviors
    void move();
    void rest();
    void printStats() override;
    void enterCombat(Enemy* enemy);
    void attack(Unit* target) override;
    void getExpGold(Enemy* target);
    void enterShop();
    void changeArmor(string name);
    void changeWeapon(string name);

private:
    string mName;
    int mGold;
    int mLevel = 1;
    int mExp;
    int x = 0;
    int y = 0;
    Equipment* mWeapon;
    Equipment* mArmor;
};

