#include "Equipment.h"

Equipment::Equipment(int power, string name)
{
	mItemPower = power;
	mName = name;
}

string Equipment::getName()
{
	return mName;
}

int Equipment::getItemPower()
{
	return mItemPower;
}
