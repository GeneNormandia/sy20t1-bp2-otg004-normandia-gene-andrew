#pragma once
#include <string>

using namespace std;

class Player;
class Enemy;

class Unit
{
public:
	Unit();

	// accessors
	int getHp();
	int setHp(int damage);
	int getPower();
	int getAgi();
	int getDex();
	string getClass();

	// behaviors
	virtual void attack(Unit* target);
	virtual void printStats();

protected:
	int mPow;
	int mVit;
	int mAgi;
	int mDex;
	int mHp;
	int mMaxHp;
	string mClass;
};

