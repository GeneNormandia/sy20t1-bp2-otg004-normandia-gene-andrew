#include "Unit.h"

Unit::Unit()
{
}

int Unit::getHp()
{
	return mHp;
}

int Unit::setHp(int damage)
{
	mHp -= damage;
	if (mHp < 0) mHp = 0;
	return mHp;
}

int Unit::getPower()
{
	return mPow;
}

int Unit::getAgi()
{
	return mAgi;
}

int Unit::getDex()
{
	return mDex;
}

string Unit::getClass()
{
	return mClass;
}

void Unit::attack(Unit* target)
{
}

void Unit::printStats()
{
}
