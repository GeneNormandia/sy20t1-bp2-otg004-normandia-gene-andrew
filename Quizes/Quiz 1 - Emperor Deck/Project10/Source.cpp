#include <iostream>
#include <string>
#include <time.h>
#include <vector>
using namespace std;

vector<string> populateDeck(int size, int round);
int playRound(int& mmLeft, int& moneyEarned, int& round);
void displayKaijiDeck(const vector<string>& emperorCards);
void displaySlaveCards(const vector<string>& slaveCards);
void pickedEmperor(vector<string>& kaijiDeck, int card, string& picked);
int playCardKaiji(int& mmLeft, int& moneyEarned, vector<string> kaijiDeck, int& round);
int betPhase(int& mmLeft, int& bet);
int playCardTonegawa(int& aiCard);
void matchResult(int& mmLeft, int& moneyEarned, vector<string>& kaijiDeck, int round, int pickedCard, int& bet, int& aiCard, string picked);
void displayStats(int& mmLeft, int& moneyEarned, int& round);
void endEvaluation(int& mmLeft, int& moneyEarned, int& round);

int main()
{
	srand(time(0));

	int round = 1;
	int mmLeft = 30;
	int moneyEarned = 0;
	//While the user still has mm left to bet or the round is less than or equal to 12, the game continues
	while (round <= 12 && mmLeft > 0 )
	{
		playRound(mmLeft, moneyEarned, round);

		round++;
	}
	//This will display the end screen when the game is over
	endEvaluation(mmLeft, moneyEarned, round);
	return 0;
}

vector<string> populateDeck(int size, int round)
{
	vector<string> cards;
	//Populates emperor deck on certain rounds
	if (round == 1 || round == 2 || round == 3 || round == 7 || round == 8 || round == 9)
	{
		cards.push_back("");
		cards.push_back("Emperor");
		cards.push_back("Citizen");
		cards.push_back("Citizen");
		cards.push_back("Citizen");
		cards.push_back("Citizen");
		return cards;
	}
	//Populates slave deck on certain rounds
	if (round == 4 || round == 5 || round == 6 || round == 10 || round == 11 || round == 12)
	{
		cards.push_back("");
		cards.push_back("Slave");
		cards.push_back("Citizen");
		cards.push_back("Citizen");
		cards.push_back("Citizen");
		cards.push_back("Citizen");
		return cards;
	}
}

//This is where the playCardKaiji function is called
int playRound(int& mmLeft, int& moneyEarned, int& round)
{
	vector<string> kaijiDeck = populateDeck(5, round);	
	if (mmLeft > 0)
	{
		playCardKaiji(mmLeft, moneyEarned, kaijiDeck, round);
		return moneyEarned;
	}
}

void displayKaijiDeck(const vector<string>& kaijiDeck)
{
	//This for loop displays the emperor deck
	for (int i = 1; i < kaijiDeck.size(); i++)
	{
		cout << "[" << i << "]" << kaijiDeck[i] << endl;
	}
}

void displaySlaveCards(const vector<string>& tonegawaDeck)
{
	//This for loop displays the slave deck 
	for (int i = 0; i < tonegawaDeck.size(); i++)
	{
		cout << "[" << i << "]" << tonegawaDeck[i] << endl;
	}
}

void pickedEmperor(vector<string>& kaijiDeck, int card, string& picked)
{
	int remove = 0;
	//This for loop removes the chosen card to play from the playCardKaiji function 
	for (int i = 1; i < kaijiDeck.size(); i++)
	{
		if (remove == card)
		{
			picked = kaijiDeck[i];
			kaijiDeck.erase(kaijiDeck.begin() + remove);
			break;
		}
		remove++;
	}
}

int playCardKaiji(int& mmLeft, int& moneyEarned, vector<string> kaijiDeck, int& round)
{
	int pickedCard = 0;
	int aiCard = 0;
	aiCard = playCardTonegawa(aiCard);
	int bet = 0;
	string picked;
	//This function display the stats
	displayStats(mmLeft, moneyEarned, round);
	//This is where the user inputs the bet
	betPhase(mmLeft, bet);
	system("cls");
	//This is where the user picks the card to be played
	cout << "Pick a card to play..." << endl;
	cout << "======================" << endl;
	displayKaijiDeck(kaijiDeck);
	cin >> pickedCard;
	pickedEmperor(kaijiDeck, pickedCard, picked);
	system("cls");
	//This function shows the evaluation of the match
	matchResult(mmLeft, moneyEarned, kaijiDeck, round, pickedCard, bet, aiCard, picked);
	system("pause");
	system("cls");
	
	return pickedCard;
}

int betPhase(int& mmLeft, int& bet)
{
	while (true)
	{
		//Input the wagered distance 
		cout << "How many mm would you like to wager, Kaiji? ";
		cin >> bet;

		if (bet > 0 && bet <= mmLeft)
		{
			mmLeft -= bet;
			break;
		}
	}
	return mmLeft;
}

//This function picks a random number to be played by the oponent
int playCardTonegawa(int& aiCard)
{
	aiCard;
	aiCard = rand() % 4;

	return aiCard;
}

//This is where the picked cards of the user and the ai are evaluated to determine the outcome
void matchResult(int& mmLeft, int& moneyEarned, vector<string>& kaijiDeck, int round, int pickedCard, int& bet, int& aiCard, string picked)
{
	int multiplier = 0;
	multiplier = bet;
	int rewardMoney;
	int rewardMoneyEmperor = 100000;
	int rewardMoneySlave = 500000;
	rewardMoneyEmperor *= bet;

	if (round == 1 || round == 2 || round == 3 || round == 7 || round == 8 || round == 9)
	{
		//If emperor is picked against citizen
		if (pickedCard == 1 && aiCard > 0)
		{
			mmLeft += multiplier;
			moneyEarned += rewardMoneyEmperor;
			cout << "Open!" << endl << endl;
			cout << "[Kaiji] Emperor vs Citizen[Tonegawa]" << endl << endl;
			cout << "You win!" << endl;
			return;
		}
		//If emperor is picked against slave
		if (pickedCard == 1 && aiCard == 0)
		{
			cout << "Open!" << endl << endl;
			cout << "[Kaiji] Emperor vs Slave [Tonegawa]" << endl << endl;
			cout << "You lose!" << endl;
			return;
		}
		
		//If citizen is picked against slave
		if (pickedCard > 1 && aiCard == 0)
		{
			mmLeft += multiplier;
			moneyEarned += rewardMoneyEmperor;
			cout << "Open!" << endl << endl;
			cout << "[Kaiji] Citizen vs Citizen[Tonegawa]" << endl << endl;
			cout << "You win!" << endl;
			return;
		}
		//If citizen is picked against citizen
		if (pickedCard > 1 && aiCard > 0)
		{
			cout << "Open!" << endl << endl;
			cout << "[Kaiji] Citizen vs Citizen [Tonegawa]" << endl << endl;
			cout << "It's a draw!" << endl;
			system("pause");
			system("cls");

			cout << "Pick a card to play..." << endl;
			cout << "======================" << endl;
			displayKaijiDeck(kaijiDeck);
			cin >> pickedCard;
			pickedEmperor(kaijiDeck, pickedCard, picked);
			system("cls");

			cout << playCardTonegawa(aiCard);
			matchResult(mmLeft, moneyEarned, kaijiDeck, round, pickedCard, bet, aiCard, picked);
			system("cls");
		}
	}
	if (round == 4 || round == 5 || round == 6 || round == 10 || round == 11 || round == 12)
	{
		//If slave is picked against citizen
		if ((pickedCard == 1 && aiCard == 1) || (pickedCard == 1 && aiCard == 2) || (pickedCard == 1 && aiCard == 3))
		{
			cout << "Open!" << endl << endl;
			cout << "[Kaiji] Slave vs Citizen [Tonegawa]" << endl << endl;
			cout << "You lose!" << endl;
			return;
		}
		//If slave is picked against emperor
		if (pickedCard == 1 && aiCard == 0)
		{
			mmLeft += multiplier;
			moneyEarned += rewardMoneySlave;
			cout << "Open!" << endl << endl;
			cout << "[Kaiji] Slave vs Emperor [Tonegawa]" << endl << endl;
			cout << "You win!" << endl;
			return;
		}
		//If citizen is picked against emperor 
		if (pickedCard > 1 && aiCard == 0)
		{
			cout << "Open!" << endl << endl;
			cout << "[Kaiji] Citizen vs Emperor [Tonegawa]" << endl << endl;
			cout << "You lose!" << endl;
			return;
		}
		//If citizen is picked against citizen
		if (pickedCard > 1 && aiCard > 0)
		{
			cout << "Open!" << endl << endl;
			cout << "[Kaiji] Citizen vs Citizen [Tonegawa]" << endl << endl;
			cout << "It's a draw!" << endl;
			system("pause");
			system("cls");

			cout << "Pick a card to play..." << endl;
			cout << "======================" << endl;
			displayKaijiDeck(kaijiDeck);
			cin >> pickedCard;
			pickedEmperor(kaijiDeck, pickedCard, picked);
			system("cls");

			cout << playCardTonegawa(aiCard);
			matchResult(mmLeft, moneyEarned, kaijiDeck, round, pickedCard, bet, aiCard, picked);
			system("cls");
		}
	}
}

//This function displays Kaiji's stats
void displayStats(int& mmLeft, int& moneyEarned, int &round)
{
	cout << "Money: " << moneyEarned << endl;
	cout << "Distance left (mm): " << mmLeft << endl;
	cout << "Round: " << round << "/12" << endl;
	//This statement displays that the user is on the emperor side on certain rounds
	if (round == 1 || round == 2 || round == 3 || round == 7 || round == 8 || round == 9)
	{
		cout << "Side: Emperor" << endl << endl;
	}
	//This statement displays that the user is on the slave side on certain rounds
	else
	{
		cout << "Side: Slave" << endl << endl;
	}
}

//This function evaluates the type of ending the user attained
void endEvaluation (int& mmLeft, int& moneyEarned, int& round)
{
	//Bad ending 
	if (mmLeft == 0)
	{
		cout << "[Kaiji] NO! I thought I had it but I was wrong!!!" << endl;
		cout << "Game over, you lost on the last round!" << endl;
	}
	//Meh ending
	if (mmLeft > 0 && moneyEarned < 20000000)
	{
		cout << "You did it, you won the game" << endl;
		cout << "Game over" << endl;
	}
	//Best ending
	else if (mmLeft > 0 && moneyEarned >= 20000000)
	{
		cout << "[Tonegawa] NO! This can't be possible! YOU! AAAHHH!!!" << endl;
		cout << "Game over, you win!" << endl;
	}
}