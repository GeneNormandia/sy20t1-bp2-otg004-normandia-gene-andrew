#include "Node.h"
#include <iostream>
#include <string>
#include <time.h>

using namespace std;

Node* createWatchers(int size);
Node* deleteWatcher(Node* watchers, int& size);
Node* passCloak(Node* watchers, int size);
Node* randomStartPoint(Node* watchers, int& size);
void printWatchers(Node* watchers, int size);

int main()
{
	srand(time(NULL));
	int size = 0;
	int round = 1;
	Node* head = new Node;

	//Get the size of the list
	cout << "How many watchers are there? ";
	cin >> size;

	//create a circular linked list
	head = createWatchers(size);

	//randomize a starting point
	head = randomStartPoint(head, size);
	system("cls");

	while (size != 1)
	{
		cout << "==========" << endl << "Round: " << round << endl << "==========" << endl;

		//display the list of watchers
		printWatchers(head, size);

		head = passCloak(head, size);
		head = deleteWatcher(head, size);

		round++;
		system("pause");
		system("cls");
	}

	//display the remaining watcher that will seek for reinforcements
	cout << "==========" << endl << "Final Result" << endl << "==========" << endl;
	cout << head->name << " will go to seek for reinforcements" << endl;

	delete head;
	head = nullptr;


	system("pause");
	return 0;
}

Node* createWatchers(int size)
{
	// Declare variables for a linked list
	Node* head = nullptr;
	Node* current = nullptr;
	Node* previous = nullptr;
	string name;

	// Loop for creating any size circular linked list
	for (int i = 0; i < size; i++)
	{
		//Ask for the names of the soldiers
		cout << "Input name for your soldier: ";
		cin >> name;

		//Create the node
		current = new Node;
		current->name = name; 

		//Set node as the head if it is the first node
		if (i == 0)
		{
			head = current;
		}

		//Set previous node's next node to the current node if applicable
		if (i > 0)
		{
			current->previous = previous;
			previous->next = current; 
		}

		//Set the current node as previous node for the next node
		previous = current; 

		//Set next node to the head node if current node is the last node
		if (i == size - 1)
		{
			current->next = head;
		}
	}

	//Set the head's previous node to the last node
	head->previous = previous;

	return head;
}

//Remove the watcher rolled from the list
Node* deleteWatcher(Node* watchers, int& size)
{
	Node* toDelete = watchers;
	Node* previous = nullptr;

	for (int i = 0; i < size; i++)
	{
		previous = toDelete;
		toDelete = toDelete->next;
	}

	previous->next = toDelete->next;
	previous = previous->next;

	//Display the eliminated watcher before removing it
	cout << toDelete->name << " was eliminated" << endl << endl;

	size -= 1;

	delete toDelete;
	return previous;
}

//Pass the cloak randomly based on list size
Node* passCloak(Node* watchers, int size)
{
	int randomWatcher = rand() % size + 1;
	int count = 0;

	cout << endl << watchers->name << " has the cloak and has drawn " << randomWatcher << endl;

	//Pass the cloak until the count matches the random number 
	while (randomWatcher != count)
	{
		watchers = watchers->next;
		count++;
	}

	return watchers;
}

//Pick a random watcher to be the starting point
Node* randomStartPoint(Node* watchers, int& size)
{
	int randomNumber = rand() % size;

	for (int i = 0; i < randomNumber; i++)
	{
		watchers = watchers->next;
	}

	return watchers;
}

//Display the remaining watchers
void printWatchers(Node* watchers, int size)
{
	Node* printWatchers = watchers;

	for (int i = 0; i < size; i++)
	{
		cout << printWatchers->name << endl;
		printWatchers = printWatchers->next;
	}
}

