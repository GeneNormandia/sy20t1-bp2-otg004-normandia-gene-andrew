#include <iostream>
#include <string>
#include <time.h>
#include "Player.h"
#include "Enemy.h"

using namespace std;

int main()
{
	srand(time(NULL));

	int stage = 1;
	int classNumber;
	string name;

	//Initialize character
	cout << "How brave of you to step up to the challenge. What's your name?" << endl;
	cin >> name;
	system("cls");
	cout << " Choose your class: 1 for Warrior, 2 for Mage, 3 for Assassin" << endl << "[1] Warrior" << endl << "[2] Mage" << endl << "[3] Assassin" << endl;
	cin >> classNumber;

	//Create player instance
	Player* player = new Player();
	player->createPlayer(name, classNumber);
	system("cls");

	while (player->getHp() > 0)
	{
		int enemyClassRandomizer = rand() % 3 + 1;
		//Initialize enemy
		Enemy* enemy = new Enemy(); 
		enemy->createEnemy(enemyClassRandomizer);

		if (stage > 1) enemy->enhanceEnemy(stage); // continue enhancing enemy for succeeding waves
		//Display the stats of the player and the enemy
		cout << "Stage: " << stage << endl;
		player->printStats();
		cout << endl << endl << "VS" << endl << endl;
		enemy->printStats();
		cout << endl;

		system("pause");
		system("cls");

		while (enemy->getHp() > 0 && player->getHp() > 0)
		{
			//The player attacks first if his AGI is higher
			if (player->getAgi() > enemy->getAgi())
			{
				player->attack(enemy);
				cout << endl;
				system("pause");
				cout << endl;

				if (enemy->getHp() > 0)
				{
					enemy->attack(player);
					cout << endl;
					system("pause");
				}
			}
			
			//Enemy attacks first because of higher AGI
			else if (player->getAgi() < enemy->getAgi()) 
			{
				enemy->attack(player);
				cout << endl;
				system("pause");
				cout << endl;

				if (player->getHp() > 0)
				{
					player->attack(enemy);
					cout << endl;
					system("pause");
				}
			}

			//Player will attack first when the AGI is equal
			else if (player->getAgi() == enemy->getAgi()) 
			{
				player->attack(enemy);
				cout << endl;
				system("pause");
				cout << endl;

				if (enemy->getHp() > 0)
				{
					enemy->attack(player);
					cout << endl;
					system("pause");
				}
			}
		}
		system("cls");
		cout << "Player HP: " << player->getHp() << endl << "Enemy HP: " << enemy->getHp() << endl;
		cout << endl;

		if (player->getHp() > 0) // call victory evaluation if player is still alive
		{
			player->gainStats(enemy);
			cout << endl;
			cout << "End of stage " << stage << endl;
			cout << endl;
			stage++;
			system("pause");
			system("cls");
		}
		//Delete enemy after the stage
		delete enemy;
	}

	//If player loses
	if (player->getHp() <= 0) 
	{
		cout << endl;
		cout << "==========" << "GAME OVER!" << endl;
		cout << player->getName() << " falls after fighting " << stage << " stage(s) in the arena" << "==========" << endl;
	}

	delete player; // deallocate player after the game
	system("pause");
	return 0;
}