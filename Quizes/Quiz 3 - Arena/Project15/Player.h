#pragma once
#include <iostream>
#include <string>

using namespace std;

class Enemy;

class Player
{
public:

	int getHp();
	int maxHp();
	int setHp(float damage);
	int getPower();
	int getVit();
	int getAgi();
	int getDex();
	string getClass();
	string getName();
	void printStats();
	void createPlayer(string name, int classNumber);
	void attack(Enemy* target); // computes hit rate 
	void doDamage(Enemy* target); // computes attack damage
	void gainStats(Enemy* target); // this evaluates the bonuses you get after victory

private:
	string mName;
	string mClass;
	int mHp;
	int mMaxHp;
	int mPower;
	int mVit;
	int mAgi;
	int mDex;
};

