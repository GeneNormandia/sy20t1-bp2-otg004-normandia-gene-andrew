#include "Player.h"
#include "Enemy.h"

string Player::getName()
{
	return mName;
}

void Player::printStats()
{
	cout << "STATS: " << endl;
	cout << "Name: " << mName << endl;
	cout << "HP: " << mHp << endl;
	cout << "Class: " << mClass << endl;
	cout << "Power: " << mPower << endl;
	cout << "Vitality: " << mVit << endl;
	cout << "Agility: " << mAgi << endl;
	cout << "Dexterity: " << mDex << endl;
}

int Player::getHp()
{
	return mHp;
}

int Player::maxHp()
{
	return mMaxHp;
}

int Player::setHp(float damage)
{
	mHp -= damage;
	if (mHp < 0) mHp = 0;
	return mHp;
}

int Player::getPower()
{
	return mPower;
}

int Player::getVit()
{
	return mVit;
}

int Player::getAgi()
{
	return mAgi;
}

int Player::getDex()
{
	return mDex;
}

string Player::getClass()
{
	return mClass;
}

void Player::createPlayer(string name, int classNumber)
{
	mName = name;

	//warrior is tanky
	if (classNumber == 1)
	{
		mClass = "Warrior";

		mHp = 18;
		mMaxHp = 18;
		mPower = 8;
		mVit = 4;
		mAgi = 3;
		mDex = 4;
	}

	//mage is balanced
	else if (classNumber == 2)
	{
		mClass = "Mage";

		mHp = 15;
		mMaxHp = 15;
		mPower = 6;
		mVit = 5;
		mAgi = 6;
		mDex = 7;
	}

	//assassin is fast, has low defense but high damage
	else if (classNumber == 3)
	{
		mClass = "Assassin";

		mHp = 12;
		mMaxHp = 12;
		mPower = 10;
		mVit = 4;
		mAgi = 9;
		mDex = 6;
	}
}

//this computes hit rate
void Player::attack(Enemy* target)
{
	int hitRate = ((float)mDex / (float)target->getAgi()) * 100;
	int probability = rand() % 100 + 1;
	if (hitRate < 20) hitRate = 20;
	else if (hitRate > 80) hitRate = 80; 

	if (probability >= 1 && probability <= hitRate)
	{
		doDamage(target);
	}

	else if (probability > hitRate)
	{
		cout << mName << " missed!" << endl;
	}

}

//attack computations
void Player::doDamage(Enemy* target)
{
	//attack bonus for warrior vs assassin
	if (mClass == "Warrior" && target->getClass() == "Assassin") 
	{
		float damage = (mPower - target->getVit()) * 1.5;
		if (damage <= 0) damage = 1;

		target->setHp(damage);
		cout << mName << " has attacked the enemy " << target->getClass() << " for " << damage << "!" << endl;
	}

	//attack bonus for assassin vs mage
	else if (mClass == "Assassin" && target->getClass() == "Mage") 
	{
		float damage = (mPower - target->getVit()) * 1.5;
		if (damage <= 0) damage = 1;

		target->setHp(damage);
		cout << mName << " has attacked the enemy " << target->getClass() << " for " << damage << "!" << endl;
	}
	//attack bonus for mage vs warrior
	else if (mClass == "Mage" && target->getClass() == "Warrior") 
	{
		float damage = (mPower - target->getVit()) * 1.5;
		if (damage <= 0) damage = 1;

		target->setHp(damage);
		cout << mName << " has attacked the enemy " << target->getClass() << " for " << damage << "!" << endl;
	}

	else
	{
		float damage = (mPower - target->getVit());
		if (damage <= 0) damage = 1;

		target->setHp(damage);
		cout << mName << " has attacked the enemy " << target->getClass() << " for " << damage << "!" << endl;
	}
}

void Player::gainStats(Enemy* target)
{
	//player heals
	mHp = mHp + ((float)mMaxHp * 0.3);
	if (mHp > mMaxHp)
	{
		mHp = mMaxHp;
	}

	if (target->getClass() == "Warrior") // rewards if you defeated a warrior
	{
		cout << "Rewards" << endl << "Power +2" << endl <<  "Vitality +2" << endl;
		mPower += 2;
		mVit += 2;
	}

	else if (target->getClass() == "Assassin") // rewards if you defeated an assassin
	{
		cout << "Rewards" << endl << "Agility +2" << endl << "Dexterity +2" << endl;
		mAgi += 2;
		mDex += 2;
	}

	if (target->getClass() == "Mage") // rewards if you defeated a mage
	{
		cout << "Reward" << endl << "Power +3" << endl;
		mPower += 3;
	}
}
