#include "Enemy.h"
#include "Player.h"

void Enemy::createEnemy(int classNumber)
{
	if (classNumber == 1) // by default, the warrior should be able to negate a large amount of attacks, but lack at offense
	{
		//warrior is tanky
		mClass = "Warrior";

		mHp = 18;
		mPower = 8;
		mVit = 4;
		mAgi = 3;
		mDex = 4;
	}

	//mage is balanced
	else if (classNumber == 2) 
	{
		mClass = "Mage";

		mHp = 15;
		mPower = 6;
		mVit = 5;
		mAgi = 6;
		mDex = 7;
	}

	//assassin is fast, has low defense but high damage
	else if (classNumber == 3) 
	{
		mClass = "Assassin";

		mHp = 12;
		mPower = 10;
		mVit = 4;
		mAgi = 9;
		mDex = 6;
	}
}

// this will compute hit rate
void Enemy::attack(Player* target)
{
	int hitRate = ((float)mDex / (float)target->getAgi()) * 100;
	int probability = rand() % 100 + 1;
	if (hitRate < 20) hitRate = 20;
	else if (hitRate > 80) hitRate = 80;

	if (probability >= 1 && probability <= hitRate) // call damage function if hit successfully
	{
		doDamage(target);
	}

	else if (probability > hitRate) // otherwise, display missed!
	{
		cout << "Enemy missed!" << endl;
	}
}

// computes attack damage
void Enemy::doDamage(Player* target)
{
	if (mClass == "Warrior" && target->getClass() == "Assassin") // bonus dmg for warrior vs assassin
	{
		float damage = (mPower - target->getVit()) * 1.5;
		if (damage <= 0) damage = 1; // minimum dmg u can deal is 1

		target->setHp(damage);
		cout << "The enemy attacked " << target->getName() << " for " << damage << "!" << endl;
	}

	else if (mClass == "Assassin" && target->getClass() == "Mage") // bonus dmg for assassin vs mage
	{
		float damage = (mPower - target->getVit()) * 1.5;
		if (damage <= 0) damage = 1;

		target->setHp(damage);
		cout << "The enemy attacked " << target->getName() << " for " << damage << "!" << endl;
	}

	else if (mClass == "Mage" && target->getClass() == "Warrior") // bonus dmg for mage vs warrior
	{
		float damage = (mPower - target->getVit()) * 1.5;
		if (damage <= 0) damage = 1;

		target->setHp(damage);
		cout << "The enemy attacked " << target->getName() << " for " << damage << "!" << endl;
	}

	else // dmg computation if no bonuses apply
	{
		float damage = (mPower - target->getVit());
		if (damage <= 0) damage = 1;

		target->setHp(damage);
		cout << "The enemy attacked " << target->getName() << " for " << damage << "!" << endl;
	}
}

// use this to make enemies stronger
void Enemy::enhanceEnemy(int stage)
{

	if (stage == 2)
	{
		mHp += ((float)mHp * 0.2);
		mPower += ((float)mPower * 0.2);
		mVit += ((float)mVit * 0.2);
		mAgi += ((float)mAgi * 0.2);
		mDex += ((float)mDex * 0.2);
	}


	else if (stage == 3)
	{
		mHp += ((float)mHp * 0.2) * 2;
		mPower += ((float)mPower * 0.2) * 2;
		mVit += ((float)mVit * 0.2) * 2;
		mAgi += ((float)mAgi * 0.2) * 2;
		mDex += ((float)mDex * 0.2) * 2;
	}

	else if (stage == 4)
	{
		mHp += ((float)mHp * 0.2) * 3;
		mPower += ((float)mPower * 0.2) * 3;
		mVit += ((float)mVit * 0.2) * 3;
		mAgi += ((float)mAgi * 0.2) * 3;
		mDex += ((float)mDex * 0.2) * 3;
	}


	else if (stage == 5)
	{
		mHp += ((float)mHp * 0.2) * 4;
		mPower += ((float)mPower * 0.2) * 4;
		mVit += ((float)mVit * 0.2) * 4;
		mAgi += ((float)mAgi * 0.2) * 4;
		mDex += ((float)mDex * 0.2) * 4;
	}


	else if (stage == 6)
	{
		mHp += ((float)mHp * 0.2) * 5;
		mPower += ((float)mPower * 0.2) * 5;
		mVit += ((float)mVit * 0.2) * 5;
		mAgi += ((float)mAgi * 0.2) * 5;
		mDex += ((float)mDex * 0.2) * 5;
	}

	else if (stage == 7)
	{
		mHp += ((float)mHp * 0.2) * 6;
		mPower += ((float)mPower * 0.2) * 6;
		mVit += ((float)mVit * 0.2) * 6;
		mAgi += ((float)mAgi * 0.2) * 6;
		mDex += ((float)mDex * 0.2) * 6;
	}

	else if (stage == 8)
	{
		mHp += ((float)mHp * 0.2) * 7;
		mPower += ((float)mPower * 0.2) * 7;
		mVit += ((float)mVit * 0.2) * 7;
		mAgi += ((float)mAgi * 0.2) * 7;
		mDex += ((float)mDex * 0.2) * 7;
	}

	else if (stage == 9)
	{
		mHp += ((float)mHp * 0.2) * 8;
		mPower += ((float)mPower * 0.2) * 8;
		mVit += ((float)mVit * 0.2) * 8;
		mAgi += ((float)mAgi * 0.2) * 8;
		mDex += ((float)mDex * 0.2) * 8;
	}

	else if (stage >= 10)
	{
		mHp += ((float)mHp * 0.2) * 9;
		mPower += ((float)mPower * 0.2) * 9;
		mVit += ((float)mVit * 0.2) * 9;
		mAgi += ((float)mAgi * 0.2) * 9;
		mDex += ((float)mDex * 0.2) * 9;
	}
}

void Enemy::printStats()
{
	cout << "Class: " << mClass << endl;
	cout << "STATS: " << endl;
	cout << "HP: " << mHp << endl;
	cout << "Power: " << mPower << endl;
	cout << "Vitality: " << mVit << endl;
	cout << "Agility: " << mAgi << endl;
	cout << "Dexterity: " << mDex << endl;
}

int Enemy::getHp()
{
	return mHp;
}

int Enemy::setHp(float damage)
{
	mHp -= damage;
	if (mHp < 0) mHp = 0;
	return mHp;
}

int Enemy::getPower()
{
	return mPower;
}

int Enemy::getVit()
{
	return mVit;
}

int Enemy::getAgi()
{
	return mAgi;
}

int Enemy::getDex()
{
	return mDex;
}

string Enemy::getClass()
{
	return mClass;
}
