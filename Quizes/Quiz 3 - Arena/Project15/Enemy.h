#pragma once
#include <iostream>
#include <string>

using namespace std;
class Player;

class Enemy
{
public:

	void createEnemy(int classNumber);
	void attack(Player* target); // computes hit rate
	void doDamage(Player* target); // computes attack damage
	void enhanceEnemy(int wave); // use this to make enemies stronger
	void printStats();
	int getHp();
	int setHp(float damage);
	int getPower();
	int getVit();
	int getAgi();
	int getDex();
	string getClass();

private:
	int mHp;
	int mPower;
	int mVit;
	int mAgi;
	int mDex;
	string mClass;
};


