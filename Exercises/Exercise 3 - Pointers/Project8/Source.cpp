#include <iostream>
#include <string>
#include <time.h>
#include <vector> 
using namespace std;

struct Item
{
    string name;
    int goldValue;
};

void printInventory(const vector<Item*> inventory)
{
    for (int i = 0; i < inventory.size(); i++)
    {
        Item* currentItem = inventory[i];
        cout << currentItem->name << " | " << currentItem->goldValue << " gold" << endl;
    }
}

Item* generateRandomItem(int& multiplier, int& totalGold)
{
    int randomNumber = rand() % 5;
    Item* createdItem = new Item;
    cout << "Looted items: " << endl;
    if (randomNumber == 0)
    {
        createdItem->name = "Cursed Stone";
        createdItem->goldValue = 0;
    }

    if (randomNumber == 1)
    {
        createdItem->name = "Jellopy";
        createdItem->goldValue = 5 * multiplier;
        totalGold += createdItem->goldValue;
    }
    if (randomNumber == 2)
    {
        createdItem->name = "Thick Leader";
        createdItem->goldValue = 25 * multiplier;
        totalGold += createdItem->goldValue;
    }
    if (randomNumber == 3)
    {
        createdItem->name = "Sharp Talon";
        createdItem->goldValue = 50 * multiplier;
        totalGold += createdItem->goldValue;
    }
    if (randomNumber == 4)
    {
        createdItem->name = "Mithril Ore";
        createdItem->goldValue = 100 * multiplier;
        totalGold += createdItem->goldValue;
    }
    return createdItem;
}

void enterDungeon(int& gold, vector<Item*>& inventory, int& multiplier, bool& enter)
{
    int totalGold = 0;
    gold -= 25;
    while (enter == 1)
    {
        Item* item = generateRandomItem(multiplier, totalGold);

        if (gold < 0)
        {
            cout << "==========" << endl << "GAME OVER!" << endl << "YOU DON'T HAVE ENOUGH MONEY!" << endl << "==========" << endl;
            return;
        }

        if (item->name == "Cursed Stone")
        {
            cout << "==========" << endl << "GAME OVER!" << endl << "YOU LOOTED A CURSED STONE!" << endl << "==========" << endl;
            for (int i = 1; i < inventory.size(); i++)
            {
                inventory.erase(inventory.begin() + i);
            }
            gold = 0;
            return;
        }
      
        else
        {
            inventory.push_back(item);
            printInventory(inventory);
            multiplier += 1;
            cout << endl << "Gold :" << gold << endl;
            cout << "Total gold collected: " << totalGold << endl;
            cout << "Do you wish to loot?" << endl << "[0] No , [1] Yes" << endl;
            cin >> enter;
            cout << endl;
            if (enter == 0)
            {
                gold += totalGold;
                return;
            }
        }
    }
}

int main()
{
    srand(time(NULL));
    vector<Item*> inventory;
    int gold = 50;
    int multiplier = 1;
    bool enter = 1;

    while (gold > 0 && gold < 500)
    {
        cout << "Gold :" << gold << endl;
        cout << "Multiplier: " << multiplier << endl;
        cout << "Do you wish to loot?" << endl << "[0] No , [1] Yes" << endl;
        cin >> enter;
        cout << endl;
        enterDungeon(gold, inventory, multiplier, enter);
    }
    
    if (gold >= 500)
    {
        cout << "==========" << endl << "CONGRATULATIONS!" << endl << "YOU WIN!" << endl << "==========" << endl;
    }

    system("pause");
    return 0;
}