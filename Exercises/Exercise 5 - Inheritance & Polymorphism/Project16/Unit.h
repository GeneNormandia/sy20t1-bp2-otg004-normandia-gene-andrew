#pragma once
#include <iostream>
#include <string>

using namespace std;

class Skill;

class Unit
{
public:
	Unit(string name, int hp, int pow, int vit, int dex, int agi);

	string getName();
	~Unit();
	int getHp();
	int getPower();
	int getVit();
	int getDex();
	int getAgi();
	Skill* getSkill();

	void setHp(int hp);
	void setPower(int pow);
	void setVit(int vit);
	void setDex(int dex);
	void setAgi(int agi);
	void cast();
	void skill();
	void setName(string name);
	void printStats();

private:
	string mName;
	int mHp;
	int mPower;
	int mVit;
	int mDex;
	int mAgi;
	Skill* mSkill;
};

