#include "Haste.h"
#include "Unit.h"
Haste::Haste()
	:Skill("Haste")
{
}

//Display the effect of the skill
void Haste::effect(Unit* player)
{
	cout << player->getName() << " used Haste!" << endl << "Agility increased 2!" << endl;
	player->setAgi(player->getAgi() + 2);
}
