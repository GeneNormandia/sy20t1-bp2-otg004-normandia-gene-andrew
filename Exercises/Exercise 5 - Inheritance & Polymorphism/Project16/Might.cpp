#include "Might.h"
#include "Unit.h"
Might:: Might()
	:Skill("Might")
{
}

//Display the effect of the skill
void Might::effect(Unit* player)
{
	cout << player->getName() << " used Might!" << endl << "Power increased by 2!" << endl;
	player->setPower(player->getPower() + 2);
}
