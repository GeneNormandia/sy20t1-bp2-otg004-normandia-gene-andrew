#pragma once
#include <string>
#include <iostream>
using namespace std;

class Unit;

class Skill
{
public:
	Skill(string name);
	string getName();
	virtual void effect(Unit* caster) = 0;

private:
	string mName;
};
