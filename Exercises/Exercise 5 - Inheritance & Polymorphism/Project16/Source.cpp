#include <iostream>
#include <string>
#include <time.h>
#include "Unit.h"

using namespace std;

int main()
{
	//Initialize the player
	string name;
	cout << "What is your name, player? ";
	cin >> name;

	Unit* player = new Unit(name, 10, 5, 3, 4, 3);
	player->printStats();

	system("pause");
	system("cls");

	//Infinite loop of randomized skills
	while (true)
	{
		//Randomly select an ability and display it
		player->skill();
		player->cast();
		player->printStats();

		delete player->getSkill();
		system("pause");
		system("cls");
	}
	
	system("pause");
	return 0;
}