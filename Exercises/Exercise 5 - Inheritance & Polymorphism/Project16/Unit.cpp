#include "Unit.h"
#include "Skill.h"
#include "Heal.h"
#include "Might.h"
#include "IronSkin.h"
#include "Concentration.h"
#include "Haste.h"

Unit::Unit(string name, int hp, int pow, int vit, int dex, int agi)
{
		mName = name;
		mHp = hp;
		mPower = pow;
		mVit = vit;
		mDex = dex;
		mAgi = agi;
}

string Unit::getName()
{
	return mName;
}

Unit::~Unit()
{
	delete mSkill;
}

int Unit::getHp()
{
	return mHp;
}

int Unit::getPower()
{
	return mPower;
}

int Unit::getVit()
{
	return mVit;
}

int Unit::getDex()
{
	return mDex;
}

int Unit::getAgi()
{
	return mAgi;
}

Skill* Unit::getSkill()
{
	return mSkill;
}

void Unit::setHp(int hp)
{
	mHp = hp;
}

void Unit::setPower(int pow)
{
	mPower = pow;
}

void Unit::setVit(int vit)
{
	mVit = vit;
}

void Unit::setDex(int dex)
{
	mDex = dex;
}

void Unit::setAgi(int agi)
{
	mAgi = agi;
}

void Unit::cast()
{
	mSkill->effect(this);
}

void Unit::skill()
{
	int randomSkill = rand() % 5;
	//Heal is the selected skill
	if (randomSkill == 0)
	{
		Skill* skill = new Heal();
		mSkill = skill;
	}
	//Might is the selected skill
	else if (randomSkill == 1)
	{
		Skill* skill = new Might();
		mSkill = skill;
	}
	//Iron Skin is the selected skill
	else if (randomSkill == 2)
	{
		Skill* skill = new IronSkin();
		mSkill = skill;
	}
	//Concentration is the selected skill
	else if (randomSkill == 3)
	{
		Skill* skill = new Concentration();
		mSkill = skill;
	}
	//Haste is the selected skill
	else if (randomSkill == 4)
	{
		Skill* skill = new Haste();
		mSkill = skill;
	}
}

void Unit::setName(string name)
{
	mName = name;
}

void Unit::printStats()
{
	cout << "==========" << endl << "STATS: " << endl;
	cout << "Name: " << mName << endl;
	cout << "HP: " << mHp << endl;
	cout << "Power: " << mPower << endl;
	cout << "Vitality: " << mVit << endl;
	cout << "Agility: " << mAgi << endl;
	cout << "Dexterity: " << mDex << endl;
}

