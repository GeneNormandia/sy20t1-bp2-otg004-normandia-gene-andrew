#include "Concentration.h"
#include "Unit.h"
Concentration::Concentration()
	:Skill("Concentration")
{
}
//Display the effect of the skill
void Concentration::effect(Unit* player)
{
	cout << player->getName() << " used Concentration!" << endl << "Dexterity increased 2!" << endl;
	player->setDex(player->getDex() + 2);
}
