#include "Heal.h"
#include "Unit.h"
Heal::Heal()
	:Skill("HEAL")
{
}

//Display the effect of the skill
void Heal::effect(Unit * player)
{
	cout << player->getName() << " healed for 10 HP!" << endl;
	player->setHp(player->getHp() + 10);
}
