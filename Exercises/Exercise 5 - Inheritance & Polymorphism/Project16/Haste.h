#pragma once
#include "Skill.h"

class Unit;

class Haste :
	public Skill
{
public:
	Haste();
	void effect(Unit* caster) override;
};


