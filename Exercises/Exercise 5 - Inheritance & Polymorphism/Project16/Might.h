#pragma once
#include "Skill.h"

class Unit;

class Might : public Skill
{
public:
	Might();
	void effect(Unit* player) override;
};