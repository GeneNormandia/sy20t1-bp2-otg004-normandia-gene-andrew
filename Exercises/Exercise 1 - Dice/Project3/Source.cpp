#include <iostream>
#include <string>
#include <time.h>
using namespace std;
int userBet(int bet, int& gold);
void diceRoll(int& dice1, int& dice2);
void payoutResult(int& gold, int bet, int dice1, int dice2, int dice3, int dice4);
void playRound(int& gold);
int main()
{
	srand(time(NULL));
	int gold = 1000;
	
	while (gold > 0)
	{
		playRound(gold);
	}
	cout << "Game Over!";
}
int userBet(int bet, int& gold)
{
	while (true)
	{
		cout << "Player gold: " << gold << endl;
		cout << "Enter a valid bet: ";
		cin >> bet;
		if (bet > 0 && bet <= gold) break;
	}
	gold -= bet;
	return bet;
}
void diceRoll(int& dice1, int& dice2)
{
	dice1 = rand() % 6 + 1;
	dice2 = rand() % 6 + 1;
}
void payoutResult(int& gold, int bet, int dice1, int dice2, int dice3, int dice4)
{
	int player = dice1 + dice2;
	int ai = dice3 + dice4;
	//Snake eyes 
	if (player == 2)
	{
		gold += bet *= 3;
		cout << "Player rolled snake eyes!" << endl;
		return;
	}
	//Draw
	if (player == ai)
	{
		gold += bet;
		cout << "Draw!" << endl;
		return;
	}
	//Player wins
	if (player > ai)
	{
		gold += bet *= 2;
		cout << "Player won!" << endl;
	}
	//Player lose
	else if (player < ai)
	{
		cout << "Ai won!" << endl;
	}
}
void playRound(int& gold)
{
	int betAmount = 0;
	int& refGold = gold;
	//input bet
	betAmount = userBet(betAmount, gold);
	int dice1Player;
	int dice2Player;
	int dice1Ai;
	int dice2Ai;
	//Result of dice
	diceRoll(dice1Player, dice2Player);
	diceRoll(dice1Ai, dice2Ai);
	cout << "Player rolled: " << dice1Player << "," << dice2Player << endl;
	cout << "Ai rolled: " << dice1Ai << "," << dice2Ai << endl;
	//Result of the round
	payoutResult(gold, betAmount, dice1Player, dice2Player, dice1Ai, dice2Ai);
	system("pause");
	system("cls");
}