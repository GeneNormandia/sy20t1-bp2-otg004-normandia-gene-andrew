#include "Wizard.h"
#include "Spell.h"

// attack function
void Wizard::attack(Wizard* target)
{
    // this will be run if MP isn't enough to cast spell
    if (this->mMp < 50)
    {
        int randomDamage = rand() % ((this->mMaxDamage - this->mMinDamage) + 1) + this->mMinDamage;
        target->mHp -= randomDamage;
        cout << mName << " has attacked " << target->mName << " for " << randomDamage << " amount of damage!" << endl;
    }

    //this will run if MP is enough to cast spell
    else if (this->mMp >= 50)
    {
        equippedSpell->activate(target, this);
    }
}

void Wizard::generateMp()
{
    int randomMp = rand() % ((20 - 10) + 1) + 10;
    this->mMp += randomMp;
    cout << mName << " has earned " << randomMp << " mana points after attacking." << endl;
}