#pragma once
#include <string>
#include <iostream>

using namespace std;

class Wizard;

class Spell
{
public:
    string mName;
    int mMinDamage;
    int mMaxDamage;
    int mMpCost;

    void activate(Wizard* target, Wizard* caster);
};