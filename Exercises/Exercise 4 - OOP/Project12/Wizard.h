#pragma once
#include <string>
#include <iostream>

using namespace std;

class Spell;

class Wizard
{
public:
	string mName;
	int mHp;
	int mMp;
	int mMinDamage;
	int mMaxDamage;
	Spell* equippedSpell;

	void attack(Wizard* target);
	void generateMp();
};