#include <iostream>
#include <string>
#include <time.h>
#include "Spell.h"
#include "Wizard.h"

using namespace std;

int main()
{
    int round = 1;

    // initialize wizard 1
    Wizard* wizard1 = new Wizard();
    wizard1->mName = "Harry";
    wizard1->mHp = 250;
    wizard1->mMp = 0;
    wizard1->mMinDamage = 10;
    wizard1->mMaxDamage = 15;

    // initialize wizard 2
    Wizard* wizard2 = new Wizard();
    wizard2->mName = "Malfoy";
    wizard2->mHp = 250;
    wizard2->mMp = 0;
    wizard2->mMinDamage = 10;
    wizard2->mMaxDamage = 15;

    // initialize Harry's spell
    Spell* spell1 = new Spell();
    spell1->mName = "Rictusempra"; // Harry's spell
    spell1->mMinDamage = 40;
    spell1->mMaxDamage = 60;
    spell1->mMpCost = 50;
    wizard1->equippedSpell = spell1; // this will equip the spell

    // initialize Malfoy's spell
    Spell* spell2 = new Spell();
    spell2->mName = "Everte Statum"; // Malfoy's spell
    spell2->mMinDamage = 40;
    spell2->mMaxDamage = 60;
    spell2->mMpCost = 50;
    wizard2->equippedSpell = spell2; // this will equip the spell

    cout << "Harry and Draco prepares to duel..." << endl;
    system("pause");
    cout << "Draco: Scared, Potter?" << endl;
    system("pause");
    cout << "Harry: You wish!" << endl;
    system("pause");
    cout << "Gilderoy: On the count of three, cast your charms to disarm your opponent, ONLY TO DISARM. We don't want any accidents here..." << endl;
    system("pause");
    cout << "Gilderoy: One.. two... three!" << endl << endl;
    system("pause");
    system("cls");

    //They will continue to duel until one wizard's Hp reaches 0
    while (wizard1->mHp > 0 && wizard2->mHp > 0)
    {
        //Display round number
        cout << "===============" << endl << "Round: " << round << endl << endl;

        //Display their attacks
        wizard1->attack(wizard2);
        wizard1->generateMp();
        cout << endl;

        wizard2->attack(wizard1);
        wizard2->generateMp();

        //Display the result of the round
        cout << "===============" << endl << "Result" << endl;
        cout << wizard1->mName << ": " << endl;
        cout << "HP: " << wizard1->mHp << endl;
        cout << "MP: " << wizard1->mMp << endl;
        cout << "---------------" << endl;
        cout << wizard2->mName << ": " << endl;
        cout << "HP: " << wizard2->mHp << endl;
        cout << "MP: " << wizard2->mMp << endl;
        system("pause");
        system("cls");

        round++;
    }

    // If one wizard reaches 0 hp 
    if (wizard1->mHp <= 0 || wizard2->mHp <= 0)
    {
        if (wizard1->mHp <= 0)
        {
            cout << wizard2->mName << " disarmed " << wizard1->mName << "!" << endl << wizard2->mName << " wins the duel!" << endl << endl;
        }

        else if (wizard2->mHp <= 0)
        {
            cout << wizard1->mName << " disarmed " << wizard2->mName << "!" << endl << wizard1->mName << " wins the duel!" << endl << endl;
     
        }
    }

    delete wizard1;
    delete wizard2;
    delete spell1;
    delete spell2;

    system("pause");
    return 0;
}