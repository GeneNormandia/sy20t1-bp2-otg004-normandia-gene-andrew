#include "Spell.h"
#include "Wizard.h"

void Spell::activate(Wizard* target, Wizard* caster)
{
    int randomDamage = rand() % ((this->mMaxDamage - this->mMinDamage) + 1) + this->mMinDamage;
    caster->mMp -= this->mMpCost; // deduct the cost of casting the spell
    target->mHp -= randomDamage;
    cout << caster->mName << " casted " << this->mName << " against " << target->mName << "!, he dealt " << randomDamage << " damage!" << endl;

}