#include <iostream>
#include <string>
#include <time.h>
#include <vector>
using namespace std;

vector<string> inventoryFill(int size);
void displayInventory(vector<string> items);
int countItem(const vector<string> items, string potion);
void removeItem(vector<string>& items, string potion);

int main()
{
	srand(time(NULL));
	string itemToCount;
	int countedItem = 0;
	string itemToRemove;
	vector<string> items = inventoryFill(10);
	//Displaying your inventory
	cout << "Your inventory:";
	displayInventory(items);
	//Count the number of a potion
	cout << "Enter a potion to count: ";
	cin >> itemToCount; 
	cout << endl;
	countedItem = countItem(items, itemToCount);
	cout << "You have " << countedItem << " " << itemToCount << " in your inventory" << endl;
	//Remove an item
	cout << "Enter a potion to remove: ";
	cin >> itemToRemove;
	removeItem(items, itemToRemove);
	//New inventory
	cout << endl << "New inventory: " << endl;
	displayInventory(items);
}
vector<string> inventoryFill(int size)
{
	string buffs[]{ "RedPotion", "Elixir", "EmptyBottle", "BluePotion" };
	vector<string> items;
	for (int i = 0; i < size; i++)
	{
		string item = buffs[rand() % 4];
		items.push_back(item);
	}
	return items;
}
void displayInventory(const vector<string> items)
{
	for (int i = 0; i < items.size(); i++)
	{
		cout << items[i] << endl;
	}
}
int countItem(const vector<string> items, string potion)
{
	int instances = 0;
	for (int i = 0; i < items.size(); i++)
	{
		if (potion == items[i])
		{
			instances++;
		}
	}
	return instances;
}
void removeItem(vector<string>& items, string potion)
{
	int remove = 0; 
	for (int i = 0; i < items.size(); i++)
	{
		if (potion == items[i])
		{
			break;
		}
		remove++;
	}
	items.erase(items.begin() + remove);
}